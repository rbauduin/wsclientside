FROM gitpod/workspace-full

# install dotnet, from

USER gitpod

# Install .NET SDK
# Source: https://docs.microsoft.com/dotnet/core/install/linux-scripted-manual#scripted-install
RUN mkdir -p /home/gitpod/dotnet && curl -fsSL https://dot.net/v1/dotnet-install.sh | bash /dev/stdin --channel 5.0 --install-dir /home/gitpod/dotnet
ENV DOTNET_ROOT=/home/gitpod/dotnet
ENV PATH=$PATH:/home/gitpod/dotnet

# End of dotnet install ##################################################

USER gitpod

RUN dotnet tool install -g dotnet-ws && echo 'PATH=$PATH:~/.dotnet/tools/' >> ~/.bashrc
